package com.zym;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zym.entity.User;
import com.zym.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestSharding {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void insertUser(){
        for (int i = 1; i <= 100; i++) {
            User user = new User();
            user.setId(i).setAge(i).setName("test"+i);
            userMapper.insert(user);
        }
    }

    @Test
    public void getAllUser(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("id");
        List<User> userList = userMapper.selectList(queryWrapper);
        for (User user : userList) {
            System.out.println(user.toString());
        }
    }
}
