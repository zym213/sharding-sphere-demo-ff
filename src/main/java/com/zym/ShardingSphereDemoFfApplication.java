package com.zym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShardingSphereDemoFfApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingSphereDemoFfApplication.class, args);
    }

}
